from setuptools import setup, find_packages

setup(
    name="torinator",
    description="TOR Endpoing Indexer",
    author="Evan Reichard",
    version="0.0.1",
    packages=find_packages("src"),
    package_dir={"": "src"},
    zip_safe=False,
    include_package_data=True,
    entry_points={"console_scripts": ["torinator = torinator:cli"]},
    install_requires=[
        "sqlalchemy",
        "requests",
        "Flask",
        "bs4",
    ],
    tests_require=["pytest"],
    extras_require={"dev": ["pre-commit", "black", "flake8", "pytest"]},
)
