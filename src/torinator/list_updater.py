import torinator
from bs4 import BeautifulSoup
from threading import Thread
from io import open
import os
import requests
import re
import time


class TORListManager:
    def __init__(self, source, cadence):
        self.__pending_shutdown = False
        self.__source = source
        self.__cadence = cadence
        self.__sources = [
            {
                "url": "https://www.dan.me.uk/tornodes",
                "filename": "danme_html",
                "name": "DanMe",
                "function": self.normalizeDanMe,
            },
            {
                "url": "https://udger.com/resources/ip-list/tor_exit_node",
                "filename": "udger_html",
                "name": "Udger",
                "function": self.normalizeUdger,
            },
        ]

    def normalizeUdger(self, raw_html):
        soup = BeautifulSoup(raw_html, "html.parser")
        all_results = soup.select('table#iptab tr a[href*="ip="]')

        normalized_results = []
        for item in all_results:
            normalized_results.append(item.text)

        return list(set(normalized_results))

    def normalizeDanMe(self, raw_html):
        matched_re = re.search(
            r"<!-- __BEGIN_TOR_NODE_LIST__ \/\/-->\n((?:.*\n)*)<!-- __END_TOR_NODE_LIST__ \/\/-->",  # noqa: E501
            raw_html,
            re.MULTILINE,
        )
        if matched_re is None:
            return None

        all_lines = matched_re[1].split("\n")
        stripped_lines = list(
            map(
                lambda x: re.sub(r"<br>$", "", x),
                filter(lambda x: x.strip() != "", all_lines),
            )
        )

        normalized_results = []
        for item in stripped_lines:
            row_cells = item.split("|")
            normalized_results.append(row_cells[0])

        return list(set(normalized_results))

    def read_file(self, filename):
        filepath = os.path.join(
            os.path.dirname(__file__), "./resources/" + filename
        )  # noqa: E501

        f = open(filepath, "r")
        return f.read()

    def read_page(self, url):
        page = requests.get(url)
        return page.text

    def __run_sources(self):
        curr_time = self.__cadence

        while not self.__pending_shutdown:
            time.sleep(1)
            curr_time += 1

            if curr_time < self.__cadence:
                continue

            curr_time = 0

            for s in self.__sources:
                # You can swap the bottom two lines for testing so you dont
                # get rate limited

                if self.__source == "web":
                    raw_html = self.read_page(s["url"])
                elif self.__source == "file":
                    raw_html = self.read_file(s["filename"])

                parsed_ips = s["function"](raw_html)
                if parsed_ips is None:
                    print(
                        "[SOURCE ERROR] You may be rate limited: %s"
                        % (s["name"])  # noqa: E501
                    )
                    continue

                torinator.database.update_tor_ips(parsed_ips, s["name"])
                print("[SOURCE UPDATE] Updated Database: %s" % (s["name"]))

    def start(self):
        self.__source_thread = Thread(target=self.__run_sources)
        self.__source_thread.start()

    def stop(self):
        self.__pending_shutdown = True
        self.__source_thread.join()
