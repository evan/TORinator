import os


def get_env(key, default=None, required=False):
    """Wrapper for gathering env vars."""
    if required:
        assert key in os.environ, "Missing Environment Variable: %s" % key
    return os.environ.get(key, default)


class Config:
    """Wrap application configurations

    Attributes
    ----------
    DB_TYPE : str
        The specied desired database (default: sqlite)
    DATA_PATH : str
        The path where to store any resources (default: ./)
    UPDATE_SOURCE : str
        Whether to use the 'file' or 'web' tor source (default: web)
    UPDATE_CADENCE : int
        Update cadence in seconds (default: 2700)
    """

    DB_TYPE = get_env("TORINATOR_DB_TYPE", default="sqlite")
    DATA_PATH = get_env("TORINATOR_DATA_PATH", default="./")
    UPDATE_SOURCE = get_env("TORINATOR_UPDATE_SOURCE", default="web")
    UPDATE_CADENCE = int(get_env("TORINATOR_UPDATE_CADENCE", default=2700))
