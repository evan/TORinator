import click
import signal
import sys
from importlib.metadata import version
from torinator.config import Config
from torinator.list_updater import TORListManager
from torinator.database import DatabaseConnector
from flask import Flask
from flask.cli import FlaskGroup

__version__ = version("torinator")

app = Flask(__name__)
database = DatabaseConnector(Config.DB_TYPE, Config.DATA_PATH)
list_manager = TORListManager(Config.UPDATE_SOURCE, Config.UPDATE_CADENCE)


def signal_handler(sig, frame):
    list_manager.stop()
    sys.exit(0)


def create_app():
    import torinator.api.v1 as api_v1

    app.register_blueprint(api_v1.bp)

    list_manager.start()
    return app


@click.group(cls=FlaskGroup, create_app=create_app)
def cli():
    """Management script for the application."""


# Handle SIGINT
signal.signal(signal.SIGINT, signal_handler)
