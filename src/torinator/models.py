from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()


class ExclusionIPAddress(Base):
    __tablename__ = "exclusion_ip_addrs"

    # ID
    id = Column(Integer, primary_key=True, unique=True)

    # IP Addr
    ip_id = Column(Integer, ForeignKey("ip_addrs.id"))
    ip_addr = relationship("IPAddress", foreign_keys=[ip_id])


class TorIPAddress(Base):
    __tablename__ = "tor_ip_addrs"

    # ID
    id = Column(Integer, primary_key=True, unique=True)

    # List Name
    list_name = Column(String)

    # IP Addr
    ip_id = Column(Integer, ForeignKey("ip_addrs.id"))
    ip_addr = relationship("IPAddress", foreign_keys=[ip_id])


class IPAddress(Base):
    __tablename__ = "ip_addrs"

    # ID
    id = Column(Integer, primary_key=True, unique=True)

    # String representation of an IP Address
    ip = Column(String, unique=True)
