import torinator
from flask import Blueprint, request

# Setup blueprint & websocket
bp = Blueprint("v1", __name__, url_prefix="/api/v1")


@bp.route("/status", methods=["GET"])
def get_status():
    """Get server version."""
    return {
        "version": torinator.__version__
        # TODO: All IPs Loaded?
    }


@bp.route("/ips", methods=["GET"])
def get_ips():
    """Get tor ips."""

    all_ips = torinator.database.get_ips()
    return {"data": all_ips, "size": len(all_ips)}


@bp.route("/exclusions", methods=["POST", "GET"])
def exclusions():

    if request.method == "GET":
        all_exclusions = torinator.database.get_exclusions()
        return {"data": all_exclusions, "size": len(all_exclusions)}
    elif request.method == "POST":
        data = request.get_json()
        torinator.database.add_exclusion(data["ip"])
        return {"message": "SUCCESS"}


@bp.route("/exclusions/<string:ip_str>", methods=["DELETE"])
def exclusions_id(ip_str):

    if request.method == "DELETE":
        torinator.database.delete_exclusion(ip_str)

    return {"message": "SUCCESS"}
